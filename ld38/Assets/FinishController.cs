﻿using UnityEngine;
using System.Collections;

public class FinishController : MonoBehaviour {

	private BoxCollider2D collider;
	
	// Use this for initialization
	void Start () {
		collider = GetComponent<BoxCollider2D>();
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.name == "Tongue" || other.gameObject.name == "Player") {
			Invoke( "ChangeLevel", 2.0f );
		}
	}
	
	void ChangeLevel() {
		Application.LoadLevel ("Start");  
	}
}
