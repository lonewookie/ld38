﻿using UnityEngine;
using System.Collections;

public class FrogController : MonoBehaviour {
	
	public bool jump = false;
	public float speed = 5.0f;
	public float move_force = 365f;
	public float max_speed = 5.0f;
	public float jump_force = 1f;
	
	private bool grounded = false;
	private Rigidbody2D rgbd;
	private BoxCollider2D collider;
	
	private float distance_to_ground;
	
	private float can_jump;
	private GameObject tongue;
	
	// Use this for initialization
	void Start () {
		rgbd = GetComponent<Rigidbody2D>();
		collider = GetComponent<BoxCollider2D>();
		tongue = GameObject.Find("Tongue");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.UpArrow) && grounded) {
			grounded = false;
			jump = true;
		}
		
		if (Input.GetMouseButton(0)) {
			drawTongue();
		} else {
			undraw();
		}
	}
	
	void drawTongue()
	{
		tongue.SetActive(true);
	
		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.enabled = true;
		
		Vector2 new_pos = new Vector2(
			Camera.main.ScreenToWorldPoint(Input.mousePosition).x, 
			Camera.main.ScreenToWorldPoint(Input.mousePosition).y
			);
		Vector2 player_pos = new Vector2(transform.position.x, transform.position.y);
		
		RaycastHit2D hit = Physics2D.Raycast(player_pos, new_pos - player_pos, 100);
			
		Vector3 new_tongue_pos = new Vector3(hit.point.x, hit.point.y, 0);
		
		tongue.transform.position = new_tongue_pos;
		
		lineRenderer.SetPosition( 0, player_pos );
		lineRenderer.SetPosition( 1, hit.point );	
	}
	
	void undraw()
	{
		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.enabled = false;
		tongue.SetActive(false);
	}
	
	void FixedUpdate() {
		float h = Input.GetAxis("Horizontal");
		if (h * rgbd.velocity.x < max_speed) {
			rgbd.AddForce(Vector2.right * h * move_force);
		}
		
		if (Mathf.Abs(rgbd.velocity.x) > max_speed) {
			rgbd.velocity = new Vector2(Mathf.Sign (rgbd.velocity.x) * max_speed, rgbd.velocity.y);
		}
		
		if (jump) {
			rgbd.AddForce(new Vector2(0f, jump_force));
			jump = false;
		}	 
	}
	
	void OnCollisionEnter2D(Collision2D other) {
		grounded = true;
		
		if (other.gameObject.name == "Tongue") {
			Physics2D.IgnoreCollision(other.collider, collider);
		}
	}
}
